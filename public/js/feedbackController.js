fdbackApp.controller('feedbackController', ['$scope', 'feedbackService','$log', '$window',
    function($scope, feedbackService, $log, $window) {

        $scope.feedback = [];
        $scope.currentPage = 1
        $scope.page = 1
        $scope.itemsPerPage = 10

        $scope.getfeedback = function() { 
            var searchData
            if ($scope.scname) {
                 searchData = {
                    scname : $scope.scname
                }
            }
            feedbackService.getFeedBacks(searchData).then(function(response) {
                 $scope.feedback  = response.feedbacks
                 $scope.totalItems = response.count.count
                 $scope.currentPage = 1
            })
        }

        $scope.submitFeedBack = function() {
            var feedbackData = {
                cname : $scope.cname,
                iname: $scope.iname,
                team: $scope.team,
                level: $scope.level,
                score: $scope.score,
                dfeedback: $scope.dfeedback
            }
            feedbackService.submitFeedBack(feedbackData).then(function(data, status){
                $window.location.href = data.redirect; 
            });

        };

         $scope.pageChanged = function(page, event) {
            $log.log('Page changed to: ' + $scope.currentPage);
            var pageData = {
                page : $scope.currentPage,
                scname : $scope.scname
            }
            feedbackService.getFeedBackPagination(pageData).then(function(response) {
                 $scope.feedback  = response
            })
         };

    }]);