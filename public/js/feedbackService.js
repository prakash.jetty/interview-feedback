fdbackApp.factory('feedbackService',
	function($http, $q, $log) {
		return {
			getFeedBacks: function(pageData) {
				var deffered = $q.defer()
				$http({
				    url: '/api/feedback/list', 
				    method: "GET",
				    params: {
				    	scname: pageData && pageData.scname ? pageData.scname : undefined
				    
				    }
				 }).then(function(response) {
					$log.debug(response)
					deffered.resolve(response.data)
				}).catch(function(response) {
					console.log("Ops: could not get any data");
					deffered.reject(response)
				});
				return deffered.promise;
			},

			submitFeedBack: function(feedbackData) {
				var deffered = $q.defer()
				$http.post('/api/feedback/submit', {
		            cname: feedbackData.cname,
		            iname: feedbackData.iname,
		            team: feedbackData.team,
		            level: feedbackData.level,
		            score: feedbackData.score,
		            dfeedback: feedbackData.dfeedback
	        	}).then(function(response) {
					$log.debug(response)
					deffered.resolve(response.data)
				}).catch(function(response) {
					$log.debug(response)
					deffered.resolve(response.data)
				});
				return deffered.promise;
			},

			getFeedBackPagination: function(pageData) {
				var deffered = $q.defer()
				$http({
				    url: '/api/feedback/list', 
				    method: "GET",
				    params: {
				    	page: pageData.page,
				    	scname: pageData.scname
				    }
				 }).then(function(response) {
					$log.debug(response)
					deffered.resolve(response.data)
				}).catch(function(response) {
					console.log("Ops: could not get any data");
					deffered.reject(response)
				});
				return deffered.promise;
			},

			searchFeedBacks: function(searchData) {
				var deffered = $q.defer()
				$http.post('/api/feedback/search', {
		            scname: searchData.scname
	        	}).then(function(response) {
	       			$log.debug(response)
					deffered.resolve(response.data)
		        }).catch(function(response) {
		            $log.error("Ops: " + data);
		            deffered.reject(response)
		        });

		        return deffered.promise;
			},

		}
	});