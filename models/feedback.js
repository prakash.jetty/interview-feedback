"use strict";

module.exports = function(sequelize, DataTypes) {
    var feedback = sequelize.define("feedback", {
        id: {
            type: DataTypes.INTEGER,
            autoIncrement: true,
            primaryKey: true
        },
        cname : {
            type : DataTypes.STRING,
            allowNull : false,
        },
        iname :  {
            type : DataTypes.STRING,
            allowNull : false,
        },
        team :  {
            type : DataTypes.STRING
        },
        level :  {
            type : DataTypes.STRING
        },
        score :  {
            type : DataTypes.STRING,
            allowNull : false,
        },
        dfeedback: {
            type : DataTypes.STRING,
            length : 4092
        },
        timestamp :  {
            type : DataTypes.DATE,
            allowNull : false,
        }
    });

    return feedback;
};