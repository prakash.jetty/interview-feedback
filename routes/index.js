

var models = require("../models");

exports.feedback = function(req, res) {
    res.render('feedback', {
        title : 'InterView feedback Form'
    });
};

exports.feedbackl = function(req, res) {
    res.render('feedbacklist', {
        title : 'InterView feedback List'
    });
};

exports.feedbacksuccess = function(req, res) {
    res.render('feedbacksuccess', {
        title : 'InterView feedback Successfully submitted'
    });
};

exports.feedbackerror = function(req, res) {
    res.render('feedbackerror', {
        title : 'InterView feedback could not be submitted'
    });
};

function getAllFeedBacks(res, response) {
    models.feedback.findAll({attributes: [[models.sequelize.fn('COUNT', models.sequelize.col('id')), 'count']]}).then(function(count){
            
            response.count = count[0]
        }).then(function() {
        models.feedback.findAll({order: 
    'timestamp DESC', limit:10, offset: currentoffset}).then(function(feedbacks){
            response.feedbacks = feedbacks
            res.json(response)
        })});
}

function getAllSerachFeedBacks(req, res, response) {
     models.feedback.findAll({
             where : [
                'cname LIKE ?',  '%' + req.query.scname + '%'
            ],
            attributes: [[models.sequelize.fn('COUNT', models.sequelize.col('id')), 'count']]
        }).then(function(count){
            
            response.count = count[0]
        }).then(function() {
        models.feedback.findAll({
            where : [
                'cname LIKE ?',  '%' + req.query.scname + '%'
            ],

            order: 'timestamp DESC',
            limit:10, 
            offset: currentoffset
        }).then(function(feedbacks){
            response.feedbacks = feedbacks
            res.json(response)
        })});
}

exports.feedbacklist = function(req, res) {
    currentoffset = !req.query.page || req.query.page == 1 ? 0 : (req.query.page-1) * 10 
    
    var response = {}
    if (!req.query.page) {
        if (!req.query.scname) {
            getAllFeedBacks(res, response)
        } else {
            getAllSerachFeedBacks(req, res, response)
        }
    } else {
        if (!req.query.scname) {
            console.log(currentoffset)
            models.feedback.findAll({order: 
        'timestamp DESC', limit:10, offset: currentoffset}).then(function(feedbacks){
                
                res.json(feedbacks)
            });
        } else {
            models.feedback.findAll({
            where : [
                'cname LIKE ?',  '%' + req.query.scname + '%'
            ],

            order: 'timestamp DESC',
            limit:10, 
            offset: currentoffset
        }).then(function(feedbacks){
            
            res.json(feedbacks)
        });
        }
    }
};

exports.feedbacksubmit = function(req, res) {
    models.feedback.create({
        cname: req.body.cname,
        iname: req.body.iname,
        team: req.body.team,
        level: req.body.level,
        score: req.body.score,
        dfeedback: req.body.dfeedback,
        timestamp: new Date()
    }).then(function(feedback){
        res.status(300).send({ redirect:"/feedbacksuccess"})
    }).catch(function(error){
        console.log("ops: " + error);
        res.status(300).send({ redirect:"/feedbackerror"})
    });
};
